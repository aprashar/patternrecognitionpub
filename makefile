CXX=g++
CXXFLAGS=-g -Wall -std=c++11
OBJ = main.o helper.o

bayesClassifier: $(OBJ)
	$(CXX) $(CXXFLAGS) -o $@ $^

clean:
	rm -rf *.o
	rm bayesClassifier
