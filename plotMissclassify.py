import matplotlib.pyplot as plt
import numpy as np
import sys
plt.xlabel('x1')
plt.ylabel('x2')
if (sys.argv[2] == "1"):
	if (sys.argv[1] == "a"):
		with open('./Samples/missClassifiedSamples1ai.txt', 'r') as f:
		    lines = f.readlines()
		    x = [float(line.split()[0]) for line in lines]
		    y = [float(line.split()[1]) for line in lines]
		plt.plot(x ,y, 'bo',ms=0.5, label="Misclasified Samples Class 1")
		with open('./Samples/missClassifiedSamples1aii.txt', 'r') as f:
		    lines = f.readlines()
		    x = [float(line.split()[0]) for line in lines]
		    y = [float(line.split()[1]) for line in lines]
		plt.plot(x ,y, 'co',ms=0.5, label="Misclasified Samples Class 2")
		line, = plt.plot([0,5,10,-5], [5,0,-5,10], color="black", label="Decision boundary")
	else:
		with open('./Samples/missClassifiedSamples1bi.txt', 'r') as f:
		    lines = f.readlines()
		    x = [float(line.split()[0]) for line in lines]
		    y = [float(line.split()[1]) for line in lines]
		plt.plot(x ,y, 'bo',ms=0.5, label="Misclasified Samples Class 1")
		with open('./Samples/missClassifiedSamples1bii.txt', 'r') as f:
		    lines = f.readlines()
		    x = [float(line.split()[0]) for line in lines]
		    y = [float(line.split()[1]) for line in lines]
		plt.plot(x ,y, 'co',ms=0.5, label="Misclasified Samples Class 2")
		line, = plt.plot([0,5,10,-5], [5+np.log(0.2/0.8),np.log(0.2/0.8),-5+np.log(0.2/0.8),10+np.log(0.2/0.8)], color="black", label="Decision boundary")
elif (sys.argv[2] == "2"):
	if (sys.argv[1] == "a"):
		with open('./Samples/missClassifiedSamples2ai.txt', 'r') as f:
		    lines = f.readlines()
		    x = [float(line.split()[0]) for line in lines]
		    y = [float(line.split()[1]) for line in lines]
		plt.plot(x ,y, 'bo',ms=0.5, label="Misclasified Samples Class 1")
		with open('./Samples/missClassifiedSamples2aii.txt', 'r') as f:
		    lines = f.readlines()
		    x = [float(line.split()[0]) for line in lines]
		    y = [float(line.split()[1]) for line in lines]
		plt.plot(x ,y, 'co',ms=0.5, label="Misclasified Samples Class 2")
		l = np.arange(-2.4069,3.5489,0.00006)
		m = np.arange(-2.4069,3.5489,0.00006)
		plt.plot(np.sqrt(np.abs((7*(m**2) - 8*m - 59.7929)/-6)),l, color="black", label="Decision boundary")
		plt.plot(-np.sqrt(np.abs((7*(m**2) - 8*m - 59.7929)/-6)),l, color="black")
	else:
		with open('./Samples/missClassifiedSamples2bi.txt', 'r') as f:
		    lines = f.readlines()
		    x = [float(line.split()[0]) for line in lines]
		    y = [float(line.split()[1]) for line in lines]
		plt.plot(x ,y, 'bo',ms=0.5, label="Misclasified Samples Class 1")
		with open('./Samples/missClassifiedSamples2bii.txt', 'r') as f:
		    lines = f.readlines()
		    x = [float(line.split()[0]) for line in lines]
		    y = [float(line.split()[1]) for line in lines]
		plt.plot(x ,y, 'co',ms=0.5, label="Misclasified Samples Class 2")
		l = np.arange(-2.376,3.523,0.00006)
		m = np.arange(-2.376,3.523,0.00006)
		plt.plot(np.sqrt(np.abs((7*(m**2) - 8*m - 59.7929 - np.log(0.2/0.8))/-6)),l, color="black", label="Decision boundary")
		plt.plot(-np.sqrt(np.abs((7*(m**2) - 8*m - 59.7929 - np.log(0.2/0.8))/-6)),l, color="black")
else:
	with open('./Samples/missClassifiedSamples3i.txt', 'r') as f:
	    lines = f.readlines()
	    x = [float(line.split()[0]) for line in lines]
	    y = [float(line.split()[1]) for line in lines]
	plt.plot(x ,y, 'bo',ms=0.5, label="Misclasified Samples Class 1")
	with open('./Samples/missClassifiedSamples3ii.txt', 'r') as f:
	    lines = f.readlines()
	    x = [float(line.split()[0]) for line in lines]
	    y = [float(line.split()[1]) for line in lines]
	plt.plot(x ,y, 'co',ms=0.5, label="Misclasified Samples Class 2")
	line, = plt.plot([0,5,10,-5], [5+np.log(0.2/0.8),np.log(0.2/0.8),-5+np.log(0.2/0.8),10+np.log(0.2/0.8)], color="black", label="Decision boundary")
plt.legend(loc='best')
plt.show()