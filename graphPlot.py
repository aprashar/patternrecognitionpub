import matplotlib.pyplot as plt
import numpy as np
import sys
plt.xlabel('x1')
plt.ylabel('x2')
if (sys.argv[2] == "1"):
	with open('./Samples/samplesOne1.txt', 'r') as f:
	    lines = f.readlines()
	    x = [float(line.split()[0]) for line in lines]
	    y = [float(line.split()[1]) for line in lines]
	plt.plot(x ,y, 'b,',ms=0.5, label="Blue Class 1")
	color = 'g'
	with open('./Samples/samplesTwo1.txt', 'r') as f:
	    lines = f.readlines()
	    x = [float(line.split()[0]) for line in lines]
	    y = [float(line.split()[1]) for line in lines]
	plt.plot(x ,y, 'c,', ms=0.5, label="Cyan Class 2")
	if (sys.argv[1] == "a"):
		line, = plt.plot([0,5,10,-5], [5,0,-5,10], color="black", label="Decision boundary")
	else:
		line, = plt.plot([0,5,10,-5], [5+np.log(0.2/0.8),np.log(0.2/0.8),-5+np.log(0.2/0.8),10+np.log(0.2/0.8)], color="black", label="Decision boundary")
	plt.legend(loc='best')
	plt.show()

elif (sys.argv[2] == "2"):
	with open('./Samples/samplesOne2.txt', 'r') as f:
	    lines = f.readlines()
	    x = [float(line.split()[0]) for line in lines]
	    y = [float(line.split()[1]) for line in lines]
	plt.plot(x ,y, 'b,',ms=0.5, label="Blue Class 1")
	color = 'g'
	with open('./Samples/samplesTwo2.txt', 'r') as f:
	    lines = f.readlines()
	    x = [float(line.split()[0]) for line in lines]
	    y = [float(line.split()[1]) for line in lines]
	plt.plot(x ,y, 'c,', ms=0.5, label="Cyan Class 2")
	if (sys.argv[1] == "a"):
		l = np.arange(-2.4069,3.5489,0.00006)
		m = np.arange(-2.4069,3.5489,0.00006)
		plt.plot(np.sqrt(np.abs((7*(m**2) - 8*m - 59.7929)/-6)),l, color="black", label="Decision boundary")
		plt.plot(-np.sqrt(np.abs((7*(m**2) - 8*m - 59.7929)/-6)),l, color="black")
	else:
		# line, = plt.plot([0,5,10,-5], [5+np.log(0.2/0.8),np.log(0.2/0.8),-5+np.log(0.2/0.8),10+np.log(0.2/0.8)], color="black")
		l = np.arange(-2.376,3.523,0.00006)
		m = np.arange(-2.376,3.523,0.00006)
		plt.plot(np.sqrt(np.abs((7*(m**2) - 8*m - 59.7929 - np.log(0.2/0.8))/-6)),l, color="black", label="Decision boundary")
		plt.plot(-np.sqrt(np.abs((7*(m**2) - 8*m - 59.7929 - np.log(0.2/0.8))/-6)),l, color="black")
	plt.legend(loc='best')
	plt.show()
else:
	with open('./Samples/samplesOne2.txt', 'r') as f:
	    lines = f.readlines()
	    x = [float(line.split()[0]) for line in lines]
	    y = [float(line.split()[1]) for line in lines]
	plt.plot(x ,y, 'b,',ms=0.5, label="Blue Class 1")
	color = 'g'
	with open('./Samples/samplesTwo2.txt', 'r') as f:
	    lines = f.readlines()
	    x = [float(line.split()[0]) for line in lines]
	    y = [float(line.split()[1]) for line in lines]
	plt.plot(x ,y, 'c,', ms=0.5, label="Cyan Class 2")
	line, = plt.plot([0,5,10,-5], [5+np.log(0.2/0.8),np.log(0.2/0.8),-5+np.log(0.2/0.8),10+np.log(0.2/0.8)], color="black", label="Decision boundary")
	plt.legend(loc='best')
	plt.show()