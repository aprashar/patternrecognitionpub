# Pattern Recognition

Repository for CS-479 UNR pattern recognition class.

## To compile the code
make
## To run the executable
./bayesClassifier
### The program makes system calls be careful when running it.
### g++ and python with matplotlib, scipy and numpy required to run the program.