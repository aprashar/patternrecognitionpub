#include "helper.hpp"

double ranf() {
	return (double)rand() / (double)RAND_MAX;
}

float box_muller(float m, float s) {				        /* mean m, standard deviation s */
	s = sqrt(s);
	float x1, x2, w, y1;
	static float y2;
	static int use_last = 0;

	if (use_last) {
		y1 = y2;
		use_last = 0;
	}
	else {
		do {
			x1 = 2.0 * ranf() - 1.0;
			x2 = 2.0 * ranf() - 1.0;
			w = x1 * x1 + x2 * x2;
		} while ( w >= 1.0 );

		w = sqrt( (-2.0 * log( w ) ) / w );
		y1 = x1 * w;
		y2 = x2 * w;
		use_last = 1;
	}

	return( m + y1 * s );
}

vector<pair<float, float>> generateSamples(int numSamples, pair <int, int> mu, vector<pair<int, int>> sigma, string outFile){
	vector<pair<float, float>> samples;
	ofstream myfile;
	myfile.open (outFile);
	for(int i = 0; i < numSamples; i++) {
		samples.push_back(make_pair(box_muller(mu.first, sigma[0].first), box_muller(mu.second, sigma[1].second)));
		myfile << samples[i].first << " " << samples[i].second << endl;
	}
	myfile.close();
	return samples;
}

vector<pair<float, float>> classifyCaseI(int numSamples, vector<pair<float, float>>sample1, vector<pair<float, float>> sample2, float pw1, float pw2, string outFile1, string outFile2){
	vector<pair<float, float>> missClassifiedSamples;
	int missedClass1 = 0, missedClass2 = 0;
	ofstream myfile;
	myfile.open (outFile1);
	for(int i = 0; i < numSamples; i++) {
		if (-sample1[i].first - sample1[i].second + 5 + log(pw1/pw2) < 0) {
			missClassifiedSamples.push_back(sample1[i]);
			myfile << sample1[i].first << " " << sample1[i].second << endl;
			missedClass1++;
		}
	}
	myfile.close();
	myfile.open (outFile2);
	for(int i = 0; i < numSamples; i++) {
		if (-sample2[i].first - sample2[i].second + 5 + log(pw1/pw2) > 0) {
			missClassifiedSamples.push_back(sample2[i]);
			myfile << sample2[i].first << " " << sample2[i].second << endl;
			missedClass2++;
		}
	}
	cout << "Number of samples missclassified for class 1 are: " << missedClass1 << endl;
	cout << "Number of samples missclassified for class 2 are: " << missedClass2 << endl;
	cout << "Number of total samples missclassified are: " << missedClass1 + missedClass2 << endl;
	myfile.close();
	return missClassifiedSamples;
}



vector<pair<float, float>> classifyCaseIII(int numSamples, vector<pair<float, float>>sample1, vector<pair<float, float>> sample2, float pw1, float pw2, string outFile1, string outFile2){
	vector<pair<float, float>> missClassifiedSamples;
	int missedClass1 = 0, missedClass2 = 0;
	ofstream myfile;
	myfile.open (outFile1);
	for(int i = 0; i < numSamples; i++) {
		if (-6*pow(sample1[i].first,2) - 7*pow(sample1[i].second,2) + 8 * sample1[i].second + 59.7259 + log(pw1/pw2) < 0) {
			missClassifiedSamples.push_back(sample1[i]);
			myfile << sample1[i].first << " " << sample1[i].second << endl;
			missedClass1++;
		}
	}
	myfile.close();
	myfile.open (outFile2);
	for(int i = 0; i < numSamples; i++) {
		if (-6*pow(sample2[i].first,2) - 7*pow(sample2[i].second,2) + 8 * sample2[i].second + 59.7259 + log(pw1/pw2) > 0) {
			missClassifiedSamples.push_back(sample2[i]);
			myfile << sample2[i].first << " " << sample2[i].second << endl;
			missedClass2++;
		}
	}
	cout << "Number of samples missclassified for class 1 are: " << missedClass1 << endl;
	cout << "Number of samples missclassified for class 2 are: " << missedClass2 << endl;
	cout << "Number of total samples missclassified are: " << missedClass1 + missedClass2 << endl;
	myfile.close();
	return missClassifiedSamples;
}

vector<pair<float, float>> classifyCaseIIIMinDist(int numSamples, vector<pair<float, float>>sample1, vector<pair<float, float>> sample2, float pw1, float pw2, string outFile1, string outFile2){
	vector<pair<float, float>> missClassifiedSamples;
	int missedClass1 = 0, missedClass2 = 0;
	ofstream myfile;
	myfile.open (outFile1);
	for(int i = 0; i < numSamples; i++) {
		if (-sample1[i].first - sample1[i].second + 5 + log(pw1/pw2) < 0) {
			missClassifiedSamples.push_back(sample1[i]);
			myfile << sample1[i].first << " " << sample1[i].second << endl;
			missedClass1++;
		}
	}
	myfile.close();
	myfile.open (outFile2);
	for(int i = 0; i < numSamples; i++) {
		if (-sample2[i].first - sample2[i].second + 5 + log(pw1/pw2) > 0) {
			missClassifiedSamples.push_back(sample2[i]);
			myfile << sample2[i].first << " " << sample2[i].second << endl;
			missedClass2++;
		}
	}
	cout << "Number of samples missclassified for class 1 are: " << missedClass1 << endl;
	cout << "Number of samples missclassified for class 2 are: " << missedClass2 << endl;
	cout << "Number of total samples missclassified are: " << missedClass1 + missedClass2 << endl;
	myfile.close();
	return missClassifiedSamples;
}