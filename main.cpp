#include <iostream>
#include <vector>
#include <math.h>
#include <cstdlib>
#include <time.h>
#include <fstream>
#include "helper.hpp"

using namespace std;

int main () {
	//** Initialize part 1 **//
	pair <int, int> muOne = make_pair(1,1);
	pair <int, int> muTwo = make_pair(4,4);
	vector<pair<int, int>> sigmaOne = {make_pair(1,0), make_pair(0,1)};
	vector<pair<int, int>> sigmaTwo = {make_pair(1,0), make_pair(0,1)};

	int numSamples = 100000;
	float pw1 = 0.5, pw2 = 0.5;
	system("mkdir Samples");


	//** Generate samples for case I **//
	vector<pair<float, float>> sample1 = generateSamples (numSamples, muOne, sigmaOne, "./Samples/samplesOne1.txt");
	vector<pair<float, float>> sample2 = generateSamples (numSamples, muTwo, sigmaTwo, "./Samples/samplesTwo1.txt");


	//** Part 1a **//
	system("python graphPlot.py a 1");
	classifyCaseI(numSamples, sample1, sample2, pw1, pw2, "./Samples/missClassifiedSamples1ai.txt", "./Samples/missClassifiedSamples1aii.txt");
	system("python plotMissclassify.py a 1");


	// //** Part 1b **//
	pw1 = 0.2;
	pw2 = 0.8;
	system("python graphPlot.py b 1");
	classifyCaseI(numSamples, sample1, sample2, pw1, pw2, "./Samples/missClassifiedSamples1bi.txt", "./Samples/missClassifiedSamples1bii.txt");
	system("python plotMissclassify.py b 1");
	system("python findMin.py 1");

	//** Clear the values for last Case **//
	sample1.clear();
	sample2.clear();

	
	// //** Initialize part 2 **//
	sigmaTwo = {make_pair(4,0), make_pair(0,8)};
	
	//** Generate samples for case III **//
	sample1 = generateSamples (numSamples, muOne, sigmaOne, "./Samples/samplesOne2.txt");
	sample2 = generateSamples (numSamples, muTwo, sigmaTwo, "./Samples/samplesTwo2.txt");

	
	//** Part 2a **//
	pw1 = 0.5;
	pw2 = 0.5;
	system("python graphPlot.py a 2");
	classifyCaseIII(numSamples, sample1, sample2, pw1, pw2, "./Samples/missClassifiedSamples2ai.txt", "./Samples/missClassifiedSamples2aii.txt");
	system("python plotMissclassify.py a 2");


	//** Part 2b **//
	pw1 = 0.2;
	pw2 = 0.8;
	system("python graphPlot.py b 2");
	classifyCaseIII(numSamples, sample1, sample2, pw1, pw2, "./Samples/missClassifiedSamples2bi.txt", "./Samples/missClassifiedSamples2bii.txt");
	system("python plotMissclassify.py b 2");
	system("python findMin.py 2");


	//** Part 3 **//
	system("python graphPlot.py - 3");
	classifyCaseIIIMinDist(numSamples, sample1, sample2, pw1, pw2, "./Samples/missClassifiedSamples3i.txt", "./Samples/missClassifiedSamples3ii.txt");
	system("python plotMissclassify.py - 3");
	return 0;
}