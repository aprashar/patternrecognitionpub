"""
===============================
Minima and roots of a function
===============================
"""
############################################################
# Define the function
############################################################
import numpy as np
import matplotlib.pyplot as plt
# import matplotlib.lines as lines
import sys

x = np.arange(0, 1, 0.01)
def f(x):
	if (sys.argv[1] == "1"):
		return np.e**(-(9*x-9*x**2))
	else:
		return np.e**(-((0.5*x*(1-x)*((9/(1+3*x))+(9/(1+7*x))))+0.5*np.log((21*x**2+10*x+1)/(32**x))))

############################################################
# Find minima
############################################################

from scipy import optimize

# Global optimization
grid = (0, 1, 0.1)
xmin_global = optimize.brute(f, (grid, ))
print("Global minima found %s" % xmin_global)

# Constrain optimization
xmin_local = optimize.fminbound(f, 0, 1)
print("Local minimum found %s" % xmin_local)

print("The value at the global minima is %s" % f(xmin_global[0]))


############################################################
# Plot function, minima, and roots
############################################################
fig = plt.figure(figsize=(6, 4))
ax = fig.add_subplot(111)

# Plot the function
ax.plot(x, f(x), 'b-', label="f(x)")

# Plot the minima
xmins = np.array([xmin_global[0], xmin_local])
ax.plot(xmins, f(xmins), 'go', label="Minima")

a = [0,xmin_global[0],xmin_global[0]]
b = [f(xmin_global[0]),f(xmin_global[0]),0]
if (sys.argv[1] == "1"):
	ax.plot(a, b,'r-', label="Chernoff bound")
else:
	ax.plot(a, b,'r--', label="Chernoff bound")	

a = [0,0.5,0.5]
b = [f(0.5),f(0.5),0]
ax.plot(a, b,'b--', label="Bhattacharyya bound")

# Decorate the figure
ax.legend(loc='best')
ax.set_xlabel('x')
ax.set_ylabel('f(x)')
ax.axhline(0, color='gray')
plt.show()