#ifndef HELPER_HPP
#define HELPER_HPP

#include <iostream>
#include <vector>
#include <math.h>
#include <fstream>
#include <cstdlib>

using namespace std;

double ranf();

float box_muller(float m, float s);

vector<pair<float, float>> generateSamples(int numSamples, pair <int, int> mu, vector<pair<int, int>> sigma, string outFile);

vector<pair<float, float>> classifyCaseI(int numSamples, vector<pair<float, float>>sample1, vector<pair<float, float>> sample2, float pw1, float pw2, string outFile1, string outFile2); //start from here
vector<pair<float, float>> classifyCaseIII(int numSamples, vector<pair<float, float>>sample1, vector<pair<float, float>> sample2, float pw1, float pw2, string outFile1, string outFile2); //start from here
vector<pair<float, float>> classifyCaseIIIMinDist(int numSamples, vector<pair<float, float>>sample1, vector<pair<float, float>> sample2, float pw1, float pw2, string outFile1, string outFile2); //start from here
#endif
